<?php
 require_once 'protected/viewc/Twig/Autoloader.php';
 Twig_Autoloader::register();
 $loader =new Twig_Loader_Filesystem('protected/view/');
 $twig = new Twig_Environment($loader, array());
 $escaper = new Twig_Extension_Escaper(false);
 $twig->addExtension($escaper);
 echo $twig->render($this->data['view'],$this->data);
 ?>
