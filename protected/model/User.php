<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Mario
 */

Doo::loadCore('db/DooModel');

class User extends DooModel {
    public $id;
    public $nombre;
    public $nomUsuario;
    public $pass;
    public $email;
    public $tipo;
    public $saldo;
    public $confirmado;
    public $token;
    
    public $_table = 'Usuario';
    public $_primarykey = 'id';
    public $_fields = array('id', 'nombre', 'nomUsuario', 'pass','email', 'tipo','saldo','confirmado','token');

}
