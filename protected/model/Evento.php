<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Evento
 *
 * @author Mario
 */
class Evento {
    public $id;
    public $idOrganizador;
    public $nombre;
    public $fecha;
    public $lugar;
    public $direccion;
    public $ciudad;
    public $maxEntradas;
    public $fisicas;
    
    public $_table = 'Evento';
    public $_primarykey = 'id';
    public $_fields = array('id', 'idOrganizador', 'nombre', 'fecha', 'lugar', 'direccion','ciudad','maxEntradas','fisicas');
}
