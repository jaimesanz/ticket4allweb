<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.d
 */

/**
 * Description of Entrada
 *
 * @author Mario
 */
class Entrada {
    public $id;
    public $numero;
    public $hash;
    public $idEvento;
    public $vendida;
    public $entrado;
    public $correo;
    public $fechaVenta;
    public $vendidaPor;
    public $entradaPor;
    
    public $_table = 'Entrada';
    public $_primarykey = 'id';
    public $_fields = array('id', 'numero', 'hash', 'idEvento', 'vendida', 'entrado','correo','fechaVenta','vendidaPor','entradaPor');
}
