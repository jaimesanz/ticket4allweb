<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function randomString($length, $mayusculas, $numeros, $caracteres) {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if ($mayusculas == 1)
            $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($numeros == 1)
            $source .= '1234567890';
        if ($caracteres == 1)
            $source .= '|@#~$%()=^*+[]{}-_';
        if ($length > 0) {
            $rstr = "";
            $source = str_split($source, 1);
            for ($i = 1; $i <= $length; $i++) {
                mt_srand((double) microtime() * 1000000);
                $num = mt_rand(1, count($source));
                $rstr .= $source[$num - 1];
            }
        }
        return $rstr;
    }

$idEvento = escapeshellarg($argv[1]);
$maxEntradas = escapeshellarg($argv[2]);


$salt = randomString(15, TRUE, TRUE, TRUE);
include_once '/service/config.php';
$con = mysqli_connect($server, $user, $pass, $bd);
if (!$con) {
    die('Could not connect: ' . mysql_error());
}
for ($i = 1; $i <= $maxEntradas; $i++) {
    $clave = hash('sha256', $salt . $i); 
    $sql="INSERT INTO Entrada (numero,hash,idEvento) VALUES (".$i.",'".$clave."',".$idEvento.")";
    mysqli_query($con,$sql);            
}
mysqli_close($con);
