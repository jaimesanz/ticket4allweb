<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CreateEventAux
 *
 * @author Mario
 */
class UCCreateEventController {

    private function randomString($length, $mayusculas, $numeros, $caracteres) {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if ($mayusculas == 1)
            $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($numeros == 1)
            $source .= '1234567890';
        if ($caracteres == 1)
            $source .= '|@#~$%()=^*+[]{}-_';
        if ($length > 0) {
            $rstr = "";
            $source = str_split($source, 1);
            for ($i = 1; $i <= $length; $i++) {
                mt_srand((double) microtime() * 1000000);
                $num = mt_rand(1, count($source));
                $rstr .= $source[$num - 1];
            }
        }
        return $rstr;
    }

    private function generaEntradas($idEvento, $maxEntradas) {
        $salt = $this->randomString(15, TRUE, TRUE, TRUE);
        Doo::loadModel('Entrada');
        $entrada = new Entrada();
        for ($i = 1; $i <= $maxEntradas; $i++) {
            $clave = hash('sha256', $salt . $i);
            $entrada->idEvento = $idEvento;
            $entrada->numero = $i;
            $entrada->hash = $clave;
            Doo::db()->insert($entrada);
        }
    }

    private function generaEntradas2($idEvento, $maxEntradas) {
        $salt = $this->randomString(15, TRUE, TRUE, TRUE);
        include 'service/config.php';
        $con = mysqli_connect($server, $user, $pass, $bd);
        if (!$con) {
            die('Could not connect: ' . mysql_error());
        }
        for ($i = 1; $i <= $maxEntradas; $i++) {
            $clave = hash('sha256', $salt . $i); 
            $sql='INSERT INTO Entrada (numero,hash,idEvento) VALUES ('.$i.',\''.$clave.'\','.$idEvento.')';
            mysqli_query($con,$sql);            
        }
        mysqli_close($con);
    }
    
     private function generaEntradas3($idEvento, $maxEntradas) {
        $salt = $this->randomString(15, TRUE, TRUE, TRUE);
        include '/service/config.php';
        $con = mysqli_connect($server, $user, $pass, $bd);
        if (!$con) {
            die('Could not connect: ' . mysql_error());
        }
        $inicio='INSERT INTO Entrada (numero,hash,idEvento) VALUES';
        $sql='';
        for ($i = 1; $i <= $maxEntradas; $i++) {
            $clave = hash('sha256', $salt . $i); 
            $sql=$sql.'('.$i.',\''.$clave.'\','.$idEvento.')';
            if($i<$maxEntradas){
                $sql=$sql.',';    
            }
        }
        $sql=$inicio.$sql;
        mysqli_query($con,$sql); 
        mysqli_close($con);
    }
    
    private function generaEntradas4($idEvento, $maxEntradas) {
        $salt = $this->randomString(15, TRUE, TRUE, TRUE);
        include 'service/config.php';
        $con = mysqli_connect($server, $user, $pass, $bd);
        if (!$con) {
            die('Could not connect: ' . mysql_error());
        }
        $inicio='INSERT INTO Entrada (numero,hash,idEvento) VALUES';
        for ($i = 1; $i <= $maxEntradas; $i++) {
            $clave = hash('sha256', $salt . $i); 
            $data[]='('.$i.',\''.$clave.'\','.$idEvento.')';
        }
        $sql=$inicio.  implode(',', $data);
        mysqli_query($con,$sql); 
        mysqli_close($con);
    }

    public function generaEvento($evento) {
        Doo::db()->insert($evento);
        $evento_bd = Doo::db()->find($evento, array('limit' => 1));
        //$this->generaEntradas($evento_bd->id, $evento_bd->maxEntradas);
        //$this->generaEntradas2($evento_bd->id, $evento_bd->maxEntradas);
        //$this->generaEntradas3($evento_bd->id, $evento_bd->maxEntradas);
        $this->generaEntradas4($evento_bd->id, $evento_bd->maxEntradas);
        //$comando="php C:\xampp\htdocs\ticket4all\protected\class\scripts\scriptEntradas.php ".$evento_bd->id." ".$evento_bd->maxEntradas.">>out.txt 2";
        //system($comando);
    }

    public function eliminarEvento($idEvento) {
        Doo::loadModel('Evento');
        Doo::loadModel('Entrada');
        $evento = new Evento();
        $entrada = new Entrada();
        $evento->id = $idEvento;
        $entrada->idEvento = $idEvento;
        Doo::db()->delete($entrada);
        Doo::db()->delete($evento);

        //Eliminar tambien todas las entradas asociadas a ese evento
    }
    
    public function crearEntradas($idEvento){
        include 'service/config.php';
        $con = mysqli_connect($server, $user, $pass, $bd);
        if (!$con) {
            die('Could not connect: ' . mysql_error());
            return 0;
        }
        $sql='SELECT nombre,lugar,fecha,direccion,ciudad FROM Evento where id='.$idEvento.';';
        //anadir a la consulta que verifique el id del usuario a ver si esta autorizado y sino retorna 0
        $result=mysqli_query($con,$sql);
        if($result!=null){
            $row=mysqli_fetch_array($result);
        }   
        $nombreEvento=$row['nombre'];
        $lugarEvento=$row['lugar'];
        $fechaEvento=$row['fecha'];
        $direccionEvento=$row['direccion'];
        $ciudadEvento=$row['ciudad'];
        $sql='SELECT numero, hash from Entrada where idEvento='.$idEvento.';';
        $result2=mysqli_query($con,$sql);
        if($result2!=null){
                ob_start();
    $num = 'CMD01-'.date('ymd');
    $nom = $nombreEvento;
    $date = '01/01/2012';
            while($row=mysqli_fetch_array($result2)){
                
                
  
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
    // get the HTML

?>
<style type="text/css">
<!--
    div.zone { border: none; border-radius: 6mm; background: #FFFFFF; border-collapse: collapse; padding:3mm; font-size: 2.7mm;}
    h1 { padding: 0; margin: 0; color: #DD0000; font-size: 7mm; }
    h2 { padding: 0; margin: 0; color: #222222; font-size: 5mm; position: relative; }
-->
</style>
<div style="font: arial;background-color: #AAAACC">
    <div style="rotate: 90; position: absolute; width: 50mm; height: 4mm; left: 195mm; top: 0; font-style: italic; font-weight: normal; text-align: center; font-size: 2.5mm;">
        Entrada generada por <a href="http://ticket4all.com/" style="color: #222222; text-decoration: none;">ticket4all</a>
    </div>
    <table style="width: 99%;border: none;" cellspacing="4mm" cellpadding="0">
        <tr>
            <td colspan="2" style="width: 100%">
                <div class="zone" style="height: 34mm;position: relative;font-size: 5mm;">
                    <div style="position: absolute; right: 3mm; top: 3mm; text-align: right; font-size: 4mm; ">
                        <b><?php echo $nom; ?></b><br>
                    </div>
                    <div style="position: absolute; right: 3mm; bottom: 3mm; text-align: right; font-size: 4mm; ">
                  
                        Precio de la entrada : <b>45,00&euro;</b><br>
                        N° entrada : <b><?php echo $row['numero']; ?></b><br>
                        Date d'achat : <b><?php echo date('d/m/Y à H:i:s'); ?></b><br>
                    </div>
                    <h1>Prueba de entrada</h1><br>
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <div class="zone" style="height: 40mm;vertical-align: middle;text-align: center;">
                    <qrcode value="<?php echo $row['hash']; ?>" ec="Q" style="width: 37mm; border: none;" ></qrcode>
                </div>
            </td>
            <td style="width: 75%">
                <div class="zone" style="height: 40mm;vertical-align: middle; text-align: justify">
                    <b>Texto de condiciones o recomendaciones</b><br>
                    Le billet est soumis aux conditions générales de vente que vous avez
                    acceptées avant l'achat du billet. Le billet d'entrée est uniquement
                    valable s'il est imprimé sur du papier A4 blanc, vierge recto et verso.
                    L'entrée est soumise au contrôle de la validité de votre billet. Une bonne
                    qualité d'impression est nécessaire. Les billets partiellement imprimés,
                    souillés, endommagés ou illisibles ne seront pas acceptés et seront
                    considérés comme non valables. En cas d'incident ou de mauvaise qualité
                    d'impression, vous devez imprimer à nouveau votre fichier. Pour vérifier
                    la bonne qualité de l'impression, assurez-vous que les informations écrites
                    sur le billet, ainsi que les pictogrammes (code à barres 2D) sont bien
                    lisibles. Ce titre doit être conservé jusqu'à la fin de la manifestation.
                    Une pièce d'identité pourra être demandée conjointement à ce billet. En
                    cas de non respect de l'ensemble des règles précisées ci-dessus, ce billet
                    sera considéré comme non valable.<br>
                    <br>
                    <i>Ce billet est reconnu électroniquement lors de votre
                    arrivée sur site. A ce titre, il ne doit être ni dupliqué, ni photocopié.
                    Toute reproduction est frauduleuse et inutile.</i>
                </div>
            </td>
        </tr>
    </table>
</div>
<?php
}
     $content = ob_get_clean();

    // convert
    require_once('html2pdf/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A5', 'es', true, 'UTF-8', 0);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('ticket.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
                                   
        }
        mysqli_close($con);
        return 1;
    }

}
