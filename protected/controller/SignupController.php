<?php

class SignupController extends DooController {

    public function index() {
        //Just replace these
        //Doo::loadCore('app/DooSiteMagic');
        //DooSiteMagic::displayHome();
        include_once ('protected/config/settings.php');
        // Comprueba que este iniciada la sesion
        $this->_application = Doo::session();
        if($this->_application->auth){
            return 'ControlPanel';
        }
        $data = [];
        $data ['url'] = $project_url;
        $data ['title'] = "Ticket4all - Registro";
        $data ['view'] = 'signup.html';
        if (isset($_GET['error'])) {
            $data ['error'] = $_GET['error'];
        }

        $this->renderc('twig', $data);
    }

    function checkBDUsuario($usuario) {
        Doo::loadModel('User');
        $user = new User();
        $user->nomUsuario = $usuario;
        $user_bd = Doo::db()->find($user, array('limit' => 1));
        if ($user_bd) {
            return 1;
        } else {
            return 0;
        }
    }

    function checkBDEmail($email) {
        Doo::loadModel('User');
        $user = new User();
        $user->email = $email;
        $user_bd = Doo::db()->find($user, array('limit' => 1));
        if ($user_bd) {
            return 1;
        } else {
            return 0;
        }
    }

    public function registro() {
        // Comprueba que este iniciada la sesion
        Doo::loadModel('User');
        $this->_application = Doo::session();
        if($this->_application->auth){
            return 'ControlPanel';
        }
        $nombre = $_POST['nombre'] . " " . $_POST['apellidos'];
        $nomUsuario = $_POST['nickname'];
        $pass = $_POST['pass'];
        $email = $_POST['correo'];
        if ($this->checkBDUsuario($nomUsuario)) {
            return 'signup?error=f8032d5cae3de20fcec887f395ec9a6a';
        } else if ($this->checkBDEmail($email)) {
            return 'signup?error=531ac50224f238df5d6efdaf36507cf2';
        } else {
            $user = new User();
            $user->nomUsuario = $nomUsuario;
            $user->nombre = $nombre;
            $user->pass = md5($pass);
            $user->email = $email;
            $user->tipo = 0;
            $user->saldo = 0;
            $result = Doo::db()->insert($user);
            $user=Doo::db()->find($user,array('limit'=>1));
            //Envio de email
            $mail = "Bienvenido a Ticket4All\n Para confirmar su cuenta pulse <a href='http://localhost/ticket4all/confirm?usID=".$user->id."'>aquí</a>";
//Titulo
            $titulo = "Confirmación reigstro Ticket4All";
//cabecera
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
//dirección del remitente 
            $headers .= "From: Administración Cuentas Ticket4All< cuentas@ticket4ll.com >\r\n";
//Enviamos el mensaje a tu_dirección_email 
            $bool = mail($user->email, $titulo, $mail, $headers);
            if ($bool) {
               // echo "Mensaje enviado";
            } else {
                //echo "Mensaje no enviado";
            }
            return '/ticket4all';
            //The result is the last inserted Id
        }
    }

}


