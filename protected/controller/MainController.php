<?php

/**
 * MainController
 * Feel free to delete the methods and replace them with your own code.
 *
 * @author darkredz
 */
class MainController extends DooController {

    
    
    public function index() {
        //Just replace these
        //Doo::loadCore('app/DooSiteMagic');
        //DooSiteMagic::displayHome();
        include_once ('protected/config/settings.php');
        // Comprueba que este iniciada la sesion
        $this->_application = Doo::session();
        if($this->_application->auth){
            return 'ControlPanel';
        }
        $data = [];
        $data ['url'] = $project_url;
        $data ['view'] = 'index.html';
        $this->renderc('twig', $data);
    }
    public function confirm() {
        include_once ('protected/config/settings.php');
        Doo::loadModel('User');
        $data = [];
        $data ['url'] = $project_url;
        $data ['view'] = 'vistaMensajes.html';
        $data ['tituloMensaje'] = 'Confirmación usuario';
        $data ['mensaje'] = 'Usuario no encontrado o no existente. Por favor comprueba que sea correcto.';
        if(isset($_GET['usID'])){
            $usuario=new User();
            $usuario->id=$_GET['usID'];
            $usuario=Doo::db()->find($usuario,array('limit'=>1));
            if(isset($usuario->confirmado)){
                $usuario->confirmado='1';
                Doo::db()->update($usuario);
                $data ['mensaje'] = 'Usuario '.$usuario->nomUsuario.' confirmado correctamente';
            } 
        }
        
        $this->renderc('twig', $data);
    }

    
    public function about() {
        include_once ('protected/config/settings.php');
        // Comprueba que este iniciada la sesion
        $this->_application = Doo::session();
        if($this->_application->auth){
            return 'ControlPanel';
        }
        $data = [];
        $data ['url'] = $project_url;
        $data ['title'] = "Ticket4all - About";
        $data ['view'] = 'about.html';
        $this->renderc('twig', $data);
    }

    public function terminos() {
        include_once ('protected/config/settings.php');
        $data = [];
        $data ['url'] = $project_url;
        $data ['title'] = "Ticket4all - Terminos";
        $data ['view'] = 'terminos.html';
        $this->renderc('twig', $data);
    }

    public function allurl() {
        Doo::loadCore('app/DooSiteMagic');
        DooSiteMagic::showAllUrl();
    }

    public function debug() {
        Doo::loadCore('app/DooSiteMagic');
        DooSiteMagic::showDebug($this->params['filename']);
    }

    public function gen_sitemap_controller() {
        //This will replace the routes.conf.php file
        Doo::loadCore('app/DooSiteMagic');
        DooSiteMagic::buildSitemap(true);
        DooSiteMagic::buildSite();
    }

    public function gen_sitemap() {
        //This will write a new file,  routes2.conf.php file
        Doo::loadCore('app/DooSiteMagic');
        DooSiteMagic::buildSitemap();
    }

    public function gen_site() {
        Doo::loadCore('app/DooSiteMagic');
        DooSiteMagic::buildSite();
    }

    public function gen_model() {
        Doo::loadCore('db/DooModelGen');
        DooModelGen::genMySQL();
    }

}

