<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminEventController
 *
 * @author Mario
 */
class AdminEventController extends DooController {
    
    function verEvento() {
        include_once ('protected/config/settings.php');
        Doo::loadModel('User');
        Doo::loadModel('Evento');
        Doo::loadModel('Portero');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }elseif (isset($_GET['id'])) {
            $evento=new Evento();
            $evento->id = $_GET['id'];
            $evento=Doo::db()->find($evento,array('limit'=>1));
            $porteros=new Portero();
            $porteros->idEvento=$evento->id;
            $porteros=Doo::db()->find($porteros);    
            $data = [];
            $data['evento'] = $evento;
            $data['porteros'] = $porteros;
            $user = $this->_application->get('user');
            $data['user'] = $user;
            $data ['url'] = $project_url;
            $data ['view'] = 'verevento.html';
            $data['title'] = 'Ticket4all - Evento';
            $this->renderc('twig', $data);
        }else{
            return 'MyEvents';
        }
    }
    public function eliminarPortero(){
        include_once('protected/config/settings.php');
        Doo::loadModel('Portero');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        $data=[];
        if(isset($_GET['idPortero'])&&isset($_GET['evento'])){
            $portero=new Portero();
            $portero->idPorterp=$_GET['idPortero'];
            $portero->idEvento=$_GET['evento'];
            Doo::db()->delete($portero);    
        }
        $data ['url'] = $project_url;
        $data ['tituloMensaje'] = 'Portero eliminado';
        $data ['view'] = 'vistaMensajes.html';
        $data ['mensaje'] = 'Portero eliminado correctamente.';
        $this->renderc('twig', $data);      
    }
    public function anadirPortero(){
        include_once('protected/config/settings.php');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        if(isset($_GET['idEvento'])){
            $evento=$_GET['idEvento'];
        }
        $data=[];
        $data ['idEvento']=$evento;
        $data ['url'] = $project_url;
        $data ['view'] = 'anadirPortero.html';
        $this->renderc('twig', $data);      
    }
    public function guardarPortero(){
        include_once('protected/config/settings.php');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        if(isset($_POST['idEvento'])&&isset($_POST['nomUsuario'])){
            Doo::loadModel('User');
            Doo::loadModel('Portero');
            $data=[];
            $portero=new Portero();
            $portero->idEvento=$_POST['idEvento'];
            $usuario = new User();
            $usuario->nomUsuario=$_POST['nomUsuario'];
            $usuario=Doo::db()->find($usuario,array('limit'=>1));
            if(isset($usuario->id)){
                $portero->idPortero=$usuario->id;
                Doo::db()->insert($portero);
                $data['mensaje']='Portero añadido correctamente';    
            } else {
                $data['mensaje']='Algo ha ido mal al añadir el portero, comprueba que el nombre de usuario es correcto';
            }
            $data['tituloMensaje']='Añadir portero';
            $data['url']=$project_url;
            $data ['view'] = 'vistaMensajes.html';
            $this->renderc('twig', $data); 
            
        }
    }
    
    public function eliminarEvento(){
        include_once ('protected/config/settings.php');
        Doo::loadModel('Evento');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }elseif (isset($_GET['id'])) {
            include_once 'protected/class/UCCreateEventController.php';
            $controladorCU = new UCCreateEventController();
            $controladorCU->eliminarEvento($_GET['id']);
            return 'MyEvents';
        }else{
            return 'MyEvents';
        }
    }
    
    public function descargaEntradas(){
        include_once ('protected/config/settings.php');
        Doo::loadModel('Evento');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }elseif ($_GET['evento']) {
            $idEvento=$_GET['evento'];
            $evento=new Evento();
            $evento->id=$idEvento;
            $evento = Doo::db()->find($evento,array('limit'=>1));
        }
        Doo::loadModel('User');
        $organizador=new User();
        $organizador->id=$this->_application->idUsuario;
        $organizador = Doo::db()->find($organizador,array('limit'=>1));
        if($organizador->id==$evento->idOrganizador){
            //generar entradas
            include_once 'protected/class/UCCreateEventController.php';
            $controladorCU = new UCCreateEventController();
            $controladorCU->crearEntradas($idEvento);
        }
        else{
            //retorna error permiso
        }
    }
}
