<?php

class SigninController extends DooController {

    public function index() {
        include_once ('protected/config/settings.php');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth){
            return 'ControlPanel';
        }
        $data = [];
        $data ['url'] = $project_url;
        $data ['title'] = "Ticket4all - Entrar";
        if (isset($_GET['error'])) {
            $data ['error'] = $_GET['error'];
        }
        $data ['view'] = 'signin.html';
        $this->renderc('twig', $data);
    }
    
    public function signin() {
        // Comprueba que este iniciada la sesion
        $this->_application = Doo::session();
        if($this->_application->auth){
            return 'ControlPanel';
        }
        if (isset($_POST['username']) && isset($_POST['password'])) {
            Doo::loadModel('User');
            $user = new User;
            $user->nomUsuario = $_POST['username'];
            $user->pass = md5($_POST['password']);
            $user_bd = Doo::db()->find( $user, array('limit'=>1) );
            if (isset($user_bd->nomUsuario)) {
                $data ['user'] = $user_bd;
                Doo::session()->start();
                $this->_application = Doo::session();
                $this->_application->auth=TRUE;
                $this->_application->user = $user_bd;
                $this->_application->idUsuario=$user_bd->id;
                return 'ControlPanel';
            } else {
                return 'signin?error=7f17f61e91948ade535981ed6eae2b66';
            }
        }  else {
            return '/ticket4all';
        }
    }
    
    public function signout(){
        Doo::session()->auth=FALSE;
        Doo::session()->destroy();
        return '/ticket4all';
    }

}
