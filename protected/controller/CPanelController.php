<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CPanelController
 *
 * @author Mario
 */
class CPanelController extends DooController {

    public function index() {
        //Just replace these
        //Doo::loadCore('app/DooSiteMagic');
        //DooSiteMagic::displayHome();
        include_once ('protected/config/settings.php');
        Doo::loadModel('User');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        $data = [];
        $user = $this->_application->get('user');
        $data['user'] = $user;
        if(isset($_GET['action'])){
        $data['action'] = $_GET['action'];
        }
        $data ['url'] = $project_url;
        $data ['view'] = 'controlpanel.html';
        $data['title'] = 'Ticket4all - Panel de Control';
        $this->renderc('twig', $data);
    }

    public function ruta_crearevento() {
        /* Despliega la vista de crear evento */
        include_once ('protected/config/settings.php');
        Doo::loadModel('User');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        $data = [];
        $user = $this->_application->get('user');
        $data['user'] = $user;
        $data ['url'] = $project_url;
        $data ['view'] = 'crearevento.html';
        $data['title'] = 'Ticket4all - Crear Evento';
        $this->renderc('twig', $data);
    }
    
    public function myevents() {
        /* Despliega la vista de crear evento */
        include_once ('protected/config/settings.php');
        Doo::loadModel('User');
        Doo::loadModel('Evento');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        $evento=new Evento();
        $evento->idOrganizador=$this->_application->get('idUsuario');
        $eventos=Doo::db()->find($evento);
        $data = [];
        $user = $this->_application->get('user');
        $data['eventos']=$eventos;
        $data['user'] = $user;
        $data ['url'] = $project_url;
        $data ['view'] = 'miseventos.html';
        $data['title'] = 'Ticket4all - Mis Eventos';
        $this->renderc('twig', $data);
    }

    public function profile(){
        include_once ('protected/config/settings.php');
        Doo::loadModel('User');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        $data = [];
        $user = $this->_application->get('user');
        $data['user'] = $user;
        $data ['url'] = $project_url;
        $data ['view'] = 'profile.html';
        $data['title'] = 'Ticket4all - Perfil';
        $this->renderc('twig', $data);
    }
    
    public function createevent(){
        /* Crea el evento en la base de datos */
        include_once ('protected/config/settings.php');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        if (isset($_POST['name']) && isset($_POST['place']) && isset($_POST['city']) && isset($_POST['date'])) {
            Doo::loadModel('Evento');
            Doo::loadModel('User');
            $evento = new Evento();
            $evento->nombre = $_POST['name'];
            $evento->direccion = $_POST['address'];
            $evento->fecha=$_POST['date'];
            $evento->lugar=$_POST['place'];
            $evento->ciudad=$_POST['city'];
            $evento->idOrganizador= $this->_application->get('idUsuario');
            $evento->maxEntradas=$_POST['max'];
            $evento->fisicas=$_POST['fisica'];
            include 'protected/class/UCCreateEventController.php';
            $controladorCU = new UCCreateEventController();
            $controladorCU->generaEvento($evento);
            
        } else {
            return "CrearEvento?action=2";//2 = error de datos
        }             
        return "ControlPanel?action=1";
    }

}

?>