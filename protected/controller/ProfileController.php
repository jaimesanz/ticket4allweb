<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminEventController
 *
 * @author Mario
 */
class ProfileController extends DooController {
    
    function editEmail(){
        include_once ('protected/config/settings.php');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        if (isset($_POST['nuevocorreo']) && isset($_POST['repetircorreo'])) {
            //Editar el e-mail
            return 'ControlPanel';
        } else {
            return "Profile";//2 = error de datos
        }
    }
    
    function editPass(){
        include_once ('protected/config/settings.php');
        $this->_application = Doo::session();
        // Comprueba que este iniciada la sesion
        if($this->_application->auth==FALSE){
            return 'signin';
        }
        if (isset($_POST['passactual']) && isset($_POST['nuevapass']) && isset($_POST['repetirnuevapass'])) {
            //Editar el e-mail
            return 'ControlPanel';
        } else {
            return "Profile";//2 = error de datos
        }
    }
}
