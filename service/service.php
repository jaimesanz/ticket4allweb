<?php 
include('lib/nusoap.php');
include('dbmanager.php');

function UsuarioEnBD($param_nomUsuario){   
    $conexion=new dbmanager();
    $sql="select * from Usuario where nomUsuario='$param_nomUsuario';";
    $esta=$conexion->executeQuery($sql);
    $resultado="0";
    if($row=mysql_fetch_array($esta)!=null){
        $resultado = "1";
    }
    return $resultado;
}
//devuelve 0 si usuario no está, 1 si es correcto y 2 si la pass esta mal
function ComprobarUsuario($param_nomUsuario, $param_pass){
    $conexion=new dbmanager();
    $sql="select * from Usuario where nomUsuario='$param_nomUsuario';";
    $esta=$conexion->executeQuery($sql);
    $resultado="0";
    if($esta!=null){
        $row=mysql_fetch_array($esta);
        if($row['pass']==$param_pass){
             $resultado="1";
        } else {
            $resultado="2";
        }
        if($row['nomUsuario']==""){
            $resultado="0";
        }
    }
    return $resultado;
}
//String separado por comas: id, idOrganizador, nombreOrganizador, nombre, fecha, direccion, ciudad, maxEntradasm fisicas, sino hay nada devuelve ""
function ObtenerEvento($param_id,$param_valor){
    $conexion=new dbmanager();
    $sql="select e.*,u.nombre as nomUsu from Usuario u, Evento e where e.id=$param_id and u.id=e.idOrganizador;";
    $result=$conexion->executeQuery($sql);
    $retorno="";
    if($result!=null){
        $row=mysql_fetch_array($result);
        if($row['id']!=""){
        $retorno=$row['id'].",".$row['idOrganizador'].",".$row['nomUsu'].",".$row['nombre'].",".$row['fecha'].",".$row['direccion'].",".$row['ciudad'].",".$row['maxEntradas'].",".$row['fisicas'];
        } else {
            $retorno="";
        }  
    }
    return $retorno;
}

function ObtenerEventosAsociados($param_nomUsuario){
    $conexion = new dbmanager();
    $sql = 'SELECT e.* FROM Evento e,Portero p, Usuario u WHERE u.nomUsuario=\''.cartonbox.'\' and p.idPortero=u.id and e.id=p.idEvento';
    $result=$conexion->executeQuery($sql);
    $resultado='';
    if($result!=null){
        while (mysql_fetch_array($result)){
            $row = mysql_fetch_array($result);
            $resultado = $resultado.$row['id'].','.$row['idOrganizador'].','.$row['nombre'].','.$row['fecha'].','.$row['lugar'].','.$row['direccion'].','.$row['ciudad'].','.$row['maxEntradas'].','.$row['fisicas'].';';
            
        }
    }
    return $resultado;
}
function ventaEntrada($param_Vendor,$param_email,$param_evento){
    $conexion = new dbmanager();
    $sql="select id from Entrada where idEvento=$param_evento and vendida=0 limit 1";
    $result = $conexion->executeQuery($sql);
    $resultado='0';
    if(result!=null){
        $row=  mysql_fetch_array($result);
        $id = intval($row['id']);
        $sql="UPDATE Entrada SET vendidaPor=$param_Vendor, correo='$param_email', fechaVenta=CURDATE(), vendida=1 WHERE id=$id";
        $conexion->executeQuery($sql);
        $resultado='1';
        include('envioEmail.php');
        envioEmail("Nombre evento",$id,"Datos evento","hash",$param_email);
    }
    return $resultado;
    
}

$server = new soap_server();
$server->configureWSDL('Servidor', 'urn:Servidor');
$server->register('UsuarioEnBD',
    array('param_nomUsuario' => 'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:Ticket4allwsdl',
    'urn:Ticket4all#UsuarioEnBD',
    'rpc',
    'encoded',
    'Retorna el datos'
);
$server->register('ventaEntrada',
    array('param_Vendor' => 'xsd:string','param_email' => 'xsd:string','param_evento' => 'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:Ticket4allwsdl',
    'urn:Ticket4all#ventaEntrada',
    'rpc',
    'encoded',
    'Retorna el datos'
);
$server->register('ComprobarUsuario',
    array('param_nomUsuario' => 'xsd:string', 'param_pass'=>'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:Ticket4allwsdl',
    'urn:Ticket4all#ComprobarUsuario',
    'rpc',
    'encoded',
    'Retorna el datos'
);
$server->register('ObtenerEvento',
    array('param_id' => 'xsd:string', 'param_valor'=>'xsd:string'),
    array('return' => 'xsd:string'),
    'urn:Ticket4allwsdl',
    'urn:Ticket4all#ObtenerEvento',
    'rpc',
    'encoded',
    'Retorna el datos'
);
$server->register('ObtenerEventosAsociados',
        array('param_nomUsuario'=>'xsd:string'),
        array('return' => 'xsd:string'),
        'urn:Ticket4allwsdl',
        'urn:Ticket4all#ObtenerEventosAsociados',
        'rpc',
        'encoded',
        'Retorna el datos'
        );

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);